# IDS 721 Mini Proj 3 [![pipeline status](https://gitlab.com/dukeaiml/IDS721/ids-mini-proj-3/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/ids-mini-proj-3/-/commits/main)

## Overview
* This repository includes the components for **Mini-Project 3 - Create an S3 Bucket using CDK with AWS CodeWhisperer**

## Goal
* Create S3 bucket using AWS CDK
* Use CodeWhisperer to generate CDK code
* Add bucket properties like versioning and encryption

## S3 Bucket Screenshot
* My S3 bucket is up and running
![S3_Bucket_List](/uploads/3a0b94739b7b40c761b1727705317233/S3_Bucket_List.png)
* The bucket has versioning enabled
![S3_Versioning](/uploads/c503893174f68f7b704e2fef736b416c/S3_Versioning.png)
* The bucket has encryption enabled
![S3_Encryption](/uploads/2d2c06e27702ad72cb3a2834596ba3f3/S3_Encryption.png)

## Key Steps
1. Sign up for Codecatalyst using this [link](https://codecatalyst.aws/explore)
2. Create an empty dev environment and select **"AWS Cloud9"**
3. Select **"Create an empty Dev Environment"** and hit **"Create"**
4. Navigate to AWS Portal and create a new user with the following permissions:
   * `AmazonS3FullAccess`
   * `AWSLambda_FullAccess`
   * `IAMFullAccess`
5. Create two inline policies for the user with the following permissions:
   * `CloudFormation`
   * `Systems Manager`
6. Finish creating the user, navigate to the "Security Credentials" tab, and create an access key
7. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION in somewhere safe
8. Navigate back to the Cloud9 environment and run `aws configure` in the terminal
9. Enter the access key and secret access key when prompted, and leave the default region name and output format empty
10. Create a project directory and navigate to it in the terminal
11. Run `cdk init app --language typescript` to create a new CDK project
12. Now you have a CDK template, you can update the `lib/` and `bin/` directory with the code from CodeWhisperer
13. After updating the code, run `npm run build` to build the project
14. Create the CloudFormation template by running `cdk synth`
15. Deploy the CloudFormation template by running `cdk deploy`
16. Navigate to the AWS Portal and check if the S3 bucket has been created

## AWS CodeWhisperer
* AWS CodeWhisperer is a tool that generates code for you
* I used the following prompts to generate the code for the S3 bucket:
  * In `lib/mini_proj3-stack.ts`, used `// make an S3 bucket` and `// make an S3 bucket and enable versioning and encryption` to generate the S3 bucket code
  * In `bin/mini_proj3.ts`, used `// add necessary variables to create the S3 bucket` to generate the necessary variables

## Reference
* [AWS CDK Documentation](https://docs.aws.amazon.com/cdk/v2/guide/hello_world.html)
